import os
SERVICE_NAME = "OrderService"
# config
PORT = int(os.environ.get("PORT", "8080"))
HOST = os.environ.get("HOST", "127.0.0.1")
MODE = os.environ.get("MODE", "dev")

QUEUE_NAME = os.environ.get("QUEUE_NAME", "inventory_queue") # Name of the queue service is "listening" on
EXCHANGE_NAME = os.environ.get("EXCHANGE_NAME", "amq.direct") # Exchange name service is connected to
PUBLISHING_PAYLOAD_TYPE = "json"
CONSUMING_PAYLOAD_TYPE = "json"
MONGODB_PORT = int(os.environ.get("MONGODB_PORT", "27017"))
MONGODB_DATABSE = os.environ.get("MONGODB_DATABASE", "inventory-service-test")

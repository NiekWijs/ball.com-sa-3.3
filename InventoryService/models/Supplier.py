from pydantic import BaseModel, Field
from typing import *

class Supplier(BaseModel):
    name: str

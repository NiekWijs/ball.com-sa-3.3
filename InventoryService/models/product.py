from odmantic import Model as OdmanticModel
from models.Supplier import Supplier

class Product(OdmanticModel):
    name: str
    description: str
    price: float
    added_by_supplier: Supplier
    in_store: int

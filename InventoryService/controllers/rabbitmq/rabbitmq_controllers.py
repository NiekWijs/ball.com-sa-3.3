import json
import pickle
import sys
from asyncio import create_task
import aio_pika
from InventoryService.main import create_rabbitmq_connection
from InventoryService.config.config import *
from InventoryService.controllers.http.http_controllers import minus_one_product_stock

async def rabbitmq_listener_aiormq(loop):
  channel = await create_rabbitmq_connection()
  declare_ok = await channel.queue_declare(QUEUE_NAME,
                                           durable=False)  # TODO: See if durable=0 is not gonna give problems
  queue = declare_ok.queue
  await channel.queue_bind(queue,
                           exchange=EXCHANGE_NAME,  # Bind the queue to the exchange
                           routing_key=QUEUE_NAME
                           )
  await channel.basic_consume(queue, on_message, no_ack=True)

async def on_message(raw_message):

  #check if message is getproduct
  message = raw_message.body.decode("utf-8")
  print(f"[*] ({raw_message.channel}) Received message: {message}")

  json_message = json.loads(message)
  event_payload_raw = json_message["event_payload"]

  for product in event_payload_raw["Products"]:
    await minus_one_product_stock(product["name"])





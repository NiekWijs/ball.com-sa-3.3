import asyncio
from datetime import datetime

import aio_pika
import aiormq
from fastapi import APIRouter, Depends, HTTPException
# from models.item import Item
from typing import *

from motor import motor_asyncio

from InventoryService.models.product import Product
from InventoryService.models.Supplier import Supplier
from fastapi import Request

from config.secrets import *
from InventoryService.config.config import *
import json
from odmantic import AIOEngine


router = APIRouter(prefix="/inventory")



db = []

# Getall products
@router.get("/")
async def get_products(request: Request):
    return await request.app.engine.find(Product, {})

# Add new product
@router.post("/")
async def add_product(request: Request, Product1: Product):
    product = Product(
      name= Product1.name,
      description= Product1.description,
      price= Product1.price,
      added_by_supplier= Supplier(name= Product1.added_by_supplier.name),
      in_store= Product1.in_store
    )

    # mongodb_uri = MONGODB_URI
    # mongodb_database = MONGODB_DATABSE
    # mongodb_port = MONGODB_PORT
    #
    # motor = motor_asyncio.AsyncIOMotorClient(mongodb_uri, mongodb_port)
    # engine = AIOEngine(motor, mongodb_database)
    # return await engine.save(product)
    return await request.app.engine.save(product)


async def minus_one_product_stock(product: str):
  mongodb_uri = MONGODB_URI
  mongodb_database = MONGODB_DATABSE
  mongodb_port = MONGODB_PORT

  motor = motor_asyncio.AsyncIOMotorClient(mongodb_uri, mongodb_port)
  engine = AIOEngine(motor, mongodb_database)

  product2 = await engine.find_one(Product, Product.name == product)
  if product2.in_store > 0:
    product2.in_store = product2.in_store-1
  else:
    print("not available!")

  await engine.save(product2)



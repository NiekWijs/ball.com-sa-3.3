from fastapi import FastAPI
from motor import motor_asyncio
from odmantic import AIOEngine
from pymongo import MongoClient
from uvicorn import run

# import models
from utils.auto_import import auto_apply_http_routes, auto_apply_rabbitmq_routers
# import from config
from config.config import PORT, HOST, MODE

# Note that this file is in the gitignore, so you have to create it yourself.
# Go to config/secrets.py and add the imported variables

# imports

from utils.ascii_banner import print_ascii_banner

import logging
import os

# create the app
app = FastAPI()
connection = None
log = logging.getLogger(__name__)
# print = lambda *args **kwargs: logger.info(*args, **kwargs)
# def print(*args, **kwargs): log.info(*args, **kwargs)



# Must be imported first for auto importing to work
from controllers.http.http_controllers import *
from controllers.rabbitmq.rabbitmq_controllers import *

# Import the modules to be passed into the auto importing functions
import controllers.rabbitmq as rabbitmq_controllers_module
import controllers.http as http_controllers_module

# auto import all controllers in controllers folder
auto_apply_http_routes(http_controllers_module, app)


# start the server
@app.on_event("startup")
async def startup():
    print_ascii_banner()
    print("[*] Starting server...")
    await auto_apply_rabbitmq_routers(rabbitmq_controllers_module)
    app.engine = create_mongodb_connection()
    app.rmq_channel = await create_rabbitmq_connection()


if __name__ == "__main__":
    run("main:app", host=HOST, port=PORT,
        reload=(MODE == "dev"), log_level=("error" if MODE != "dev" else "debug"))


async def create_rabbitmq_connection(rabbitmq_uri=RABBITMQ_URL):
  connection = await aiormq.connect(rabbitmq_uri)
  channel = await connection.channel()
  return channel

def create_mongodb_connection(mongodb_uri=MONGODB_URI,
                              mongodb_database=MONGODB_DATABSE, mongodb_port=MONGODB_PORT):
  mongodb = MongoClient(mongodb_uri, mongodb_port)[mongodb_database]
  motor = motor_asyncio.AsyncIOMotorClient(mongodb_uri, mongodb_port)
  db = motor[mongodb_database]
  engine = AIOEngine(motor, mongodb_database)
  return engine

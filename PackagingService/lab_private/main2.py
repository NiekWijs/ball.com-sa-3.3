from repositories.package_order_repo import  PackageOrderRepository
from models.PackageOrder import PackageOrder
from models.product import Product
from models.customer import Customer
from models.address import Address
from config.secrets import *


def main():
    package_repository = PackageOrderRepository()
    products = [Product("Product 1"),Product("Product 2"), Product("Product 3")]
    address = Address(street="123 Main St", city="Anytown", country="NL", postal_code="90210")
    customer = Customer(name="jhon doe", email="jhondoe@mail.com", phone="123453")

    package_order = PackageOrder()
    package_order_id = package_repository.get_package_order_id()

    package_order.create_package_order(id= package_order_id, products=products, address=address, customer=customer)

    print(package_order.status)
    package_order.package_package_order()
    print(package_order.status)


if __name__ == "__main__":
    main()
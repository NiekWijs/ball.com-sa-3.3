from datetime import datetime

from pydantic import BaseModel
from models.address import Address


class Customer(BaseModel):
    name: str
    email: str
    phone: str
    address: Address

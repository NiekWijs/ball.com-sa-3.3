from datetime import datetime

from pydantic import BaseModel, Field
from typing import *

from models.product import Product
from models.customer import Customer
from models.address import Address

from odmantic import Model as OdmanticModel
from odmantic import Field


from eventsorcery.aggregate import Aggregate


class Order(Aggregate):
    products: List[Product] = Field(max_items=20)
    date: datetime
    ordered_by: Customer
    address: Address

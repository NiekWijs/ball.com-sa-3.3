from uvicorn import run
from fastapi import FastAPI

# import models
from utils.auto_import import auto_apply_http_routes, auto_apply_rabbitmq_routers
# import from config
from config.config import PORT, HOST, MODE

# Note that this file is in the gitignore, so you have to create it yourself.
# Go to config/secrets.py and add the imported variables

from utils.ascii_banner import print_ascii_banner

# create the app
app = FastAPI()

# Must be imported first for auto importing to work
from controllers.http.http_controllers import *
from controllers.rabbitmq.rabbitmq_controllers import *

# Import the modules to be passed into the auto importing functions
import controllers.rabbitmq as rabbitmq_controllers_module
import controllers.http as http_controllers_module

from pymongo import MongoClient
from motor import motor_asyncio
from odmantic import AIOEngine
from config.config import *
from config.secrets import *

# auto import all controllers in controllers folder
auto_apply_http_routes(http_controllers_module, app)


async def create_rabbitmq_connection(rabbitmq_uri=RABBITMQ_URL):
    try:
        connection = await aiormq.connect(rabbitmq_uri)
        channel = await connection.channel()
        return channel
    except Exception as e:
        print(f"[!] Could not connect to rabbitmq: {str(e)}")
        return None

def create_mongodb_connection(mongodb_uri=MONGODB_URI,
                              mongodb_database=MONGODB_DATABSE, mongodb_port=MONGODB_PORT):
    mongodb = MongoClient(mongodb_uri, mongodb_port)[mongodb_database]
    motor = motor_asyncio.AsyncIOMotorClient(mongodb_uri, mongodb_port)
    db = motor[mongodb_database]
    engine = AIOEngine(motor, mongodb_database)
    return engine

@app.on_event("startup")
async def startup():
    print_ascii_banner()
    print("[*] Starting server...")
    await auto_apply_rabbitmq_routers(rabbitmq_controllers_module)
    app.engine = create_mongodb_connection()
    app.rmq_channel = await create_rabbitmq_connection()
    globals()["engine"] = app.engine
    globals()["rmq_channel"] = app.rmq_channel


if __name__ == "__main__":
    run("main:app", host=HOST, port=PORT,
        reload=(MODE == "dev"), log_level=("error" if MODE != "dev" else "debug"))
import json
import pickle
import sys
from asyncio import create_task

import aio_pika



import aiormq

from config.config import *
from config.secrets import *


# All listeners should start with "rabbitmq_listener*"
from main import create_rabbitmq_connection


async def rabbitmq_listener_aiormq(loop):
    channel = await create_rabbitmq_connection()
    if channel:
        declare_ok = await channel.queue_declare(QUEUE_NAME,
                                                 durable=False) # TODO: See if durable=0 is not gonna give problems
        queue = declare_ok.queue
        await channel.queue_bind(queue,
                                 exchange=EXCHANGE_NAME, # Bind the queue to the exchange
                                 routing_key=QUEUE_NAME
                                 )
        async def on_message(raw_message):
            message = raw_message.body.decode("utf-8")
            print(f"[*] ({raw_message.channel}) Received message: {message}")
            json_message = json.loads(message)

            # event_payload_raw = json_message["event_payload"]
            # if CONSUMING_PAYLOAD_TYPE == "pickle":
            #     event_payload_serialized = bytes.fromhex(event_payload_raw)
            #     deserialized_object = pickle.loads(event_payload_serialized)
            #     event_payload = deserialized_object
            # else:
            #     event_payload = json.dumps(event_payload_raw)


            # print(event_payload)

            # await raw_message.channel.basic_ack(raw_message.delivery.delivery_tag)

        await channel.basic_consume(queue, on_message, no_ack=True)


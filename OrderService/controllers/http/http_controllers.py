import asyncio
import json
import pickle
from datetime import datetime

import aio_pika
import aiormq
from fastapi import APIRouter, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from typing import *

from config.config import *
from models.order import Order
from models.product import Product
from models.customer import Customer
from models.address import Address
from config.secrets import *
from fastapi import Request


router = APIRouter(prefix="/products")



@router.get("/")
async def get_orders(request: Request):
    return await request.app.engine.find(Order)


@router.post("/")
async def create_order(order: Order, request: Request):

    await request.app.engine.save(order)


    # connection = await aiormq.connect(RABBITMQ_URL)
    # channel = await connection.channel()
    channel = request.app.rmq_channel
    if channel:
        message_dict = {
            "event_name": "OrderCreatedEvent",
            "event_date": str(datetime.now()),
            # This is suuper duber unsecure, some guardrails should be added
            "event_payload":
                pickle.dumps(order).hex() if PUBLISHING_PAYLOAD_TYPE == "pickle"
                else jsonable_encoder(order)
        }
        # message = str(message_dict).replace("'",'"').encode()
        message = json.dumps(message_dict).encode()
        await channel.basic_publish(
            message,
            exchange=EXCHANGE_NAME, # Exchange name service is connected to
            routing_key=QUEUE_NAME, # Queue name message is being sent to
        )
        print("[*] Published message to RabbitMQ")
    return order






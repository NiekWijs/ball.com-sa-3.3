from pymodm.connection import connect

from pymodm import MongoModel, fields
from pymongo.write_concern import WriteConcern

connect(
    "mongodb://mogumogu:JdtCnwy93DmQ3Q@159.223.225.254:27017/order-management-test?authSource=admin&readPreference=primary&ssl=false&directConnection=true",
    alias="my-app")


class product(MongoModel):
    name: fields.CharField()
    description: fields.CharField()
    price: fields.IntegerField()

    class Meta:
        write_concern = WriteConcern(j=True)
        connection_alias = 'my-app'


product("ball", "Ronde ball ban leer", 10).save()

import os

SERVICE_NAME = "OrderService"

# config
PORT = int(os.environ.get("PORT", "5100"))
HOST = os.environ.get("HOST", "0.0.0.0")
MODE = os.environ.get("MODE", "dev")


QUEUE_NAME = os.environ.get("QUEUE_NAME", "queue_name_1") # Name of the queue service is "listening" on
EXCHANGE_NAME = os.environ.get("EXCHANGE_NAME", "amq.direct") # Exchange name service is connected to

PUBLISHING_PAYLOAD_TYPE = "json"
CONSUMING_PAYLOAD_TYPE = "json"

MONGODB_PORT = int(os.environ.get("MONGODB_PORT", "27017"))
MONGODB_DATABSE = os.environ.get("MONGODB_DATABASE", "order-management-db")

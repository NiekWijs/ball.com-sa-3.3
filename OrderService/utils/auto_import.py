import types
from asyncio import gather, get_event_loop, create_task

from fastapi import APIRouter


async def auto_apply_rabbitmq_routers(module):
    loop = get_event_loop()
    async def apply(submodule, func_args=[loop]):
        coroutines = [
            create_task(getattr(submodule, func)(*func_args)) for func in dir(submodule)
            if isinstance(getattr(submodule, func), types.FunctionType)
               and func.startswith("rabbitmq_listener")
        ]
        gather(*coroutines)
    await apply_function_to_auto_import(module, apply)


def auto_apply_http_routes(module, app):
    def apply(submodule, func_args=[app]):
        router = submodule.router
        print(f"Added fastapi router from: {submodule.__name__}  with prefix: {router.prefix} with {len(router.routes)} routes")
        app.include_router(router)
        if isinstance(router, APIRouter):
            for route in router.routes:
                print(f"Added route: {route.methods} {route.path}")
    apply_function_to_auto_import(module, apply)


def apply_function_to_auto_import(module, apply_func,
                                  func_args=None,
                                  func_kwargs=None):
    if not func_args: func_args = []
    if not func_kwargs: func_kwargs = {}
    for submodule in autoimport_submodule(module):
        return apply_func(submodule, *func_args, **func_kwargs)


def autoimport_submodule(module):
    for submodule_str in dir(module):
        submodule = getattr(module, submodule_str)
        if isinstance(submodule, types.ModuleType):
            yield submodule

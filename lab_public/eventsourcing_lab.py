import asyncio
import os
# from pydantic import BaseModel

from motor import motor_asyncio
from odmantic import AIOEngine
from pydantic import Required
from pydantic.color import Optional
from pymongo import MongoClient

from eventsorcery.aggregate import Aggregate
from eventsorcery.fields import SumField, SetField

# sqlalchemy setup
from lab_public.eventsorcery.database_strategies.odmantic_strategy import OdmanticStrategy

# odmantic
from odmantic import Field

from odmantic import Model as OdmanticModel

MONGODB_PORT = int(os.environ.get("MONGODB_PORT", "27017"))
MONGODB_DATABSE = os.environ.get("MONGODB_DATABASE", "adb")
MONGODB_URI = "mongodb://mogumogu:JdtCnwy93DmQ3Q@159.223.225.254/"


def create_mongodb_connection(mongodb_uri=MONGODB_URI,
							  mongodb_database=MONGODB_DATABSE, mongodb_port=MONGODB_PORT):
	mongodb = MongoClient(mongodb_uri, mongodb_port)[mongodb_database]
	motor = motor_asyncio.AsyncIOMotorClient(mongodb_uri, mongodb_port)
	# motor.get_io_loop = asyncio.get_event_loop
	db = motor[mongodb_database]
	engine = AIOEngine(motor, mongodb_database)
	return engine


## Models  setup

class WalletEvent(OdmanticModel):
	aggregate_id: Optional[int]
	sequence: Optional[int]
	amount: Optional[int]
	status: Optional[str]


class WalletSnapshot(OdmanticModel):
	aggregate_id: Optional[int] = Field(primary_field=True)
	sequence: Optional[int]
	balance: int
	status: str


class WalletAggregate(Aggregate):
	# If we had more time i'd try adding this dynamically
	class Meta:
		strategy = OdmanticStrategy(
			session=create_mongodb_connection())
		event_model = WalletEvent
		snapshot_model = WalletSnapshot
	balance = SumField("amount")
	status = SetField("status")


wallet = WalletAggregate(1)
wallet.add_event(WalletEvent(amount=10, status="aaaa"))
wallet.add_event(WalletEvent(amount=5, status="bbbb"))
wallet.add_event(WalletEvent(amount=5, status="cccc"))
wallet.commit()
print(wallet.balance)

wallet.create_snapshot()

wallet2 = WalletAggregate(1)
print(wallet2.balance)

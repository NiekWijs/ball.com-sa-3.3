from eventsorcery.base_strategy import BaseStrategy
from typing import Any, Optional, List
from eventsorcery.event import Event
from asyncio import get_event_loop


class OdmanticStrategy(BaseStrategy):
	def __init__(self, session):
		self.run_async = get_event_loop().run_until_complete
		self.session = session

	@staticmethod
	def to_event(obj: object, **kwargs) -> Event:
		return Event(**{k: v
						for k, v
						in obj.__dict__.items()
						if not k.startswith("_")})

	@staticmethod
	def to_object(event: Event, **kwargs) -> object:
		model = kwargs.get("model")
		return model(**event._clean())


	def get_events(self,
				   aggregate_id: Any,
				   sequence: Any = 0,
				   **kwargs) -> List[dict]:
		# Really annoyingly whe have to get the model param from kwargs
		Model = kwargs.get("model")
		# get data
		if sequence:
			events = self.run_async(
				self.session.find(Model, Model.aggregate_id == aggregate_id,
							  Model.sequence > sequence,
							  sort=Model.sequence.asc())
			)
		else:
			events = self.run_async(
				self.session.find(Model,
							  	Model.aggregate_id == aggregate_id,
							  	sort=Model.sequence.asc()))
		return list(events)

	def get_latest_snapshot(self, aggregate_id: Any, **kwargs) -> Optional[Event]:
		# # TODO: Check if this actually works correctly
		Model = kwargs.get("model")
		snapshot = self.run_async(
			self.session.find_one(Model,
							  Model.aggregate_id == aggregate_id))
		return snapshot


	def save_event(self, event, **kwargs) -> None:
		Model = kwargs.get("model")
		to_model = self.to_object(event, model=Model)
		self.run_async(self.session.save(to_model))

	def save_snapshot(self, event, **kwargs) -> None:
		# Update the event in the aggregate table
		Model = kwargs.get("model")
		to_model = self.to_object(event, model=Model)
		self.run_async(self.session.save(to_model))
